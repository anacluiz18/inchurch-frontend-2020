import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User, ApiData } from 'src/app/models/user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  private usersApiData: ApiData;
  public users: User;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers() {
    this.userService.read().subscribe(
      res => {
        this.usersApiData = res
        this.users = res.data
      }
    );
  }

  create(){
    this.router.navigate(['/user/create']);
  }
}
