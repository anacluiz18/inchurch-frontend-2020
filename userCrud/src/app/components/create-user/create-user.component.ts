import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { ResponseCreate } from 'src/app/models/user.model';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  public newUserForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    job: new FormControl('', [Validators.required])
  });

  createdUser: ResponseCreate;

  constructor(private userService:UserService, private router: Router) { }

  ngOnInit(): void {
  }

  getFormControl(prop) {
    return this.newUserForm.get(prop);
  }

  save(){
    this.userService.create(this.newUserForm.value).subscribe(
      res => {
        this.createdUser = res;
        alert('User created successfully.')
      }
    )
  }

  backToHome(){
    this.router.navigate(['/'])
  }

}
