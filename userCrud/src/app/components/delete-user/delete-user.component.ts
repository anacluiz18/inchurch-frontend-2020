import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.scss']
})
export class DeleteUserComponent implements OnInit {
  private id: string;
  public user: User;

  constructor(private userService: UserService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.userService.readOne(this.id).subscribe(
      res => {
        this.user = res.data
        console.log(this.user)
      }
    )
  }

  deleteUser(){
    this.userService.delete(this.id).subscribe(
      res => {
        alert('User successfully removed')
      }
    )
  }

  backToHome(){
    this.router.navigate(['/'])
  }

}
