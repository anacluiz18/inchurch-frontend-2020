import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { RequestUpdate } from 'src/app/models/user.model';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit {
  private id: string;
  public requestUserUpdate: RequestUpdate;
  
  public updateUserForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    job: new FormControl('', [Validators.required])
  });

  constructor(private userService: UserService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id')
    this.userService.readOne(this.id).subscribe(
      res => {
        this.requestUserUpdate = {
          name: `${res.data.first_name} ${res.data.last_name}`,
          job: ''
        }
      }
    )
  }

  getFormControl(prop) {
    return this.updateUserForm.get(prop);
  }

  update(){
    this.userService.update(this.id, this.updateUserForm.value).subscribe(
      res => {
        alert(`Updated successfully. Name: ${res.name}, Job: ${res.job}.`)
      }
    )
  }

  backToHome(){
    this.router.navigate(['/'])
  }

}
