import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResponseUpdate, ApiData, ResponseUser, RequestCreate, ResponseCreate, RequestUpdate } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = 'https://reqres.in/api/users'

  constructor(private http: HttpClient) { }

  // GET all users 
  read(): Observable<ApiData> {
    return this.http.get<ApiData>(this.baseUrl)
  }

  // GET only one user
  readOne(id: string): Observable<ResponseUser>{
    const url = `${this.baseUrl}/${id}`
    return this.http.get<ResponseUser>(url)
  }

  // CREATE a new user
  create(newUProps: RequestCreate): Observable<ResponseCreate> {
    return this.http.post<ResponseCreate>(this.baseUrl, newUProps)
  }

  // UPDATE a user
  update(id: string, updateUBody: RequestUpdate): Observable<ResponseUpdate>{
    const url = `${this.baseUrl}/${id}`
    return this.http.put<ResponseUpdate>(url, updateUBody)
  }

  // DELETE a user
  delete(id: string): Observable<any>{
    return this.http.delete<any>(this.baseUrl)
  }
}
